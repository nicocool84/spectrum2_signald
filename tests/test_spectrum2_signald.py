import asyncio
import logging

import pytest

from spectrum2_signald import __version__
from spectrum2_signald.backend import SignalBackend, Session
from spectrum2_signald.signald import SpectrumClient


def test_version():
    assert __version__ == "0.1.4"


# async def get_signald() -> SpectrumClient:
#     loop = asyncio.get_running_loop()
#     _, signald = await loop.create_unix_connection(
#         SpectrumClient, path=SIGNALD_SOCKET_PATH
#     )
#     return signald


# @pytest.mark.asyncio
# async def test_get_roster():
#     spectrum = SignalBackend(SIGNALD_SOCKET_PATH, "signal.example.com", "signal.cfg")
#     roster = await spectrum.get_roster()


SIGNALD_SOCKET_PATH = "/var/run/signald/signald.sock"
logging.basicConfig(level=logging.DEBUG)
